﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Coursework_1
{
    //Author: David Frame
    //Class purpose: Create institutions that an attendee can be assosiated with.
    //Last updated: 24/10/2016
    public class Institution
    {
        private string institutionname = "";
        private string institutionaddress = "";

        public string InstitutionName
        {
            get
            {
                return institutionname;
            }
            set
            {
                institutionname = value;
            }
        }

        public string Institutionaddress
        {
            get
            {
                return institutionaddress;
            }
            set
            {
                institutionaddress = value;
            }
        }
    }
}
