﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Coursework_1
{
    //Author: David Frame
    //Class purpose: Create attendees, inheriting from the person class. 
    //Last updated: 24/10/2016
    public class Attendee: Person
    {
        //Attributes
        private int attendeeref = 0;
        private string conferencename = "";
        private string registrationtype = "";
        private bool paid = false;
        private bool presenter = false;
        private string papertitle = "";
        Institution inst;

        public Attendee() : base() //Create a new institution called inst
        {
            inst = new Institution();
        }

        //Get and Set Methods with validation using exceptions
        public int AttendeeRef
        {
            get
            {
                return attendeeref;
            }
            set
            {
                if (value < 40000 || value > 60000)
                {
                    throw new ArgumentException("Out of range, value must be between 40000 and 60000 inclusivley.");
                }
                attendeeref = value;
            }
        }

        public Institution Inst
        {
            get
            {
                return inst;
            }
        }

        public string ConferenceName
        {
            get
            {
                return conferencename;
            }
            set
            {
                if (value == "")
                {
                    throw new ArgumentException("Missing data, conference name must be submitted.");
                }
                conferencename = value;
            }
        }

        public string RegistrationType
        {
            get
            {
                return registrationtype;
            }
            set
            {
                registrationtype = value;
            }
        }

        public bool Paid
        {
            get
            {
                return this.paid;
            }
            set
            {
                this.paid = value;
            }
        }

        public bool Presenter
        {
            get
            {
                return this.presenter;
            }
            set
            {
                this.presenter = value;
            }
        }

        public string PaperTitle
        {
            get
            {
                return this.papertitle;
            }
            set
            {
                if (presenter == true && value == "")
                {
                    throw new ArgumentException("Missing data, paper title must be submitted.");
                }
                this.papertitle = value;
            }
        }

        //This method returns the cost of attending the conference for the Attendee that it is passed.
        public double getCost()
        {
            //Works out the pricing for the attendee based on their registration type
            int workingCost = 0;
            if (this.registrationtype == "Full")
            {
                workingCost = 500;
            }
            else if (this.registrationtype == "Student")
            {
                workingCost =  300;
            }
            else if (this.registrationtype == "Organiser")
            {
                return 0;
            }

            if (this.Presenter) //If the user is a presenter, reduce their cost by 10%
            {
                return workingCost * 0.9;
            }
            else
            {
                return workingCost;
            }
        }
    }
}
