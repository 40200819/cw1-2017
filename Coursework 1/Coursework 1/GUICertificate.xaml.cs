﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Coursework_1
{
    //Author: David Frame
    //Class purpose: Display the certificate window.
    //Last updated: 24/10/2016
    public partial class GUICertificate : Window
    {
        public GUICertificate(String firstname, String secondname, String conferencename, String papertitle, bool presenter)
        {
            InitializeComponent();

            StringBuilder s = new StringBuilder();
            if (!presenter) //Change the certificate wording depending on if the attendee was a presenter
            {
                s.AppendFormat("This is to certify that {0} {1} attended {2}", firstname, secondname, conferencename);
                tbkCertificate.Text = s.ToString();
            }
            else
            {
                s.AppendFormat("This is to certify that {0} {1} attended {2} and presented a paper entitled {3}", firstname, secondname, conferencename, papertitle);
                tbkCertificate.Text = s.ToString();
            }
        }
    }
}
