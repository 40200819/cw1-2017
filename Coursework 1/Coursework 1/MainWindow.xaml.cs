﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Coursework_1
{
    //Author: David Frame
    //Class purpose: Display the main window and basic functionality like clearing its fields.
    //Last updated: 24/10/2016
    public partial class MainWindow : Window
    {
        Attendee att1 = new Attendee(); //Create a new instance of attendee called 'att1'
        public MainWindow()
        {
            InitializeComponent();
        }

        private void chkPresenter_Click(object sender, RoutedEventArgs e)
        {
            if(tbxPaperTitle.IsEnabled)
            {
                tbxPaperTitle.Text = "";
                tbxPaperTitle.IsEnabled = false; //If the person is not a presenter, disable the paper title field on the form.
            }
            else
            {
                tbxPaperTitle.IsEnabled = true;
            }
        }

        private void btnClear_Click(object sender, RoutedEventArgs e) //Set the form's fields to blank/default values
        {
            tbx1Name.Text = "";
            tbx2Name.Text = "";
            tbxRef.Text = "";
            tbxInstName.Text = "";
            tbxConName.Text = "";
            cmbRegType.Text = "Full";
            chkPaid.IsChecked = false;
            chkPresenter.IsChecked = false;
            tbxPaperTitle.IsEnabled = false;
            tbxPaperTitle.Text = "";
        }

        private void btnSet_Click(object sender, RoutedEventArgs e)
        {
            
            try //Attempt to set the variables of the attendee instance 'att1' to the values in the form
            {
                att1.FirstName = tbx1Name.Text;
                att1.SecondName = tbx2Name.Text;
                att1.AttendeeRef = int.Parse(tbxRef.Text);
                att1.Inst.InstitutionName = tbxInstName.Text;
                att1.ConferenceName = tbxConName.Text;
                att1.RegistrationType = cmbRegType.Text;
                att1.Paid = chkPaid.IsChecked.Value;
                att1.Presenter = chkPresenter.IsChecked.Value;
                att1.PaperTitle = tbxPaperTitle.Text;
                btnInvoice.IsEnabled = true;
                btnCertificate.IsEnabled = true;
            }
            catch(Exception ex) //If the attempt fails, tell the user what they did wrong
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void btnGet_Click(object sender, RoutedEventArgs e)
        {
            //Set the form's fields to the attendee object's values 
            tbx1Name.Text = att1.FirstName;
            tbx2Name.Text = att1.SecondName;
            tbxRef.Text = att1.AttendeeRef.ToString();
            tbxInstName.Text = att1.Inst.InstitutionName;
            tbxConName.Text = att1.ConferenceName;
            cmbRegType.Text = att1.RegistrationType;
            chkPaid.IsChecked = att1.Paid;
            chkPresenter.IsChecked= att1.Presenter;
            tbxPaperTitle.Text = att1.PaperTitle;
        }

        private void btnInvoice_Click(object sender, RoutedEventArgs e)
        {
            //Create an instance of the invoice window
            GUIInvoice Inv = new GUIInvoice(att1.FirstName, att1.SecondName, att1.ConferenceName, att1.Inst.InstitutionName, att1.getCost());
            Inv.Show();
        }

        private void btnCertificate_Click(object sender, RoutedEventArgs e)
        {
            //Create an instance of the certificate window
            GUICertificate Cer = new GUICertificate(att1.FirstName, att1.SecondName, att1.ConferenceName, att1.PaperTitle, att1.Presenter);
            Cer.Show();
        }
    }
}
