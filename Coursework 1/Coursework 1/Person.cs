﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Coursework_1
{
    //Author: David Frame
    //Class purpose: Parent class that defines basic attributes of people. To be inherited by the attendee class.
    //Last updated: 24/10/2016
    public class Person
    {
        protected string firstname = ""; //Protected so that attendee can access it
        protected string secondname = "";

        public Person() //Parent of attendee
        {
        }

        public string FirstName
        {
            get
            {
                return firstname;
            }
            set
            {
                if (value == "") //Data validation
                {
                    throw new ArgumentException("Missing data, first name must be submitted.");
                }
                firstname = value;
            }
        }

        public string SecondName
        {
            get
            {
                return secondname;
            }
            set
            {
                if (value == "")
                {
                    throw new ArgumentException("Missing data, second name must be submitted.");
                }
                secondname = value;
            }
        }
    }
}
