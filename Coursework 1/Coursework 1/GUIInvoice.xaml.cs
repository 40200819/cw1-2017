﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Coursework_1
{
    //Author: David Frame
    //Class purpose: Display the invoice window.
    //Last updated: 24/10/2016
    public partial class GUIInvoice : Window
    {
        public GUIInvoice(String firstname, String secondname, String conferencename, String institutionname, double cost)
        {
            //Creates and then fills the Invoice form
            InitializeComponent();
            lblInvName.Content = "Name: " + firstname + " " + secondname;
            lblInvInsti.Content = "Institution: " + institutionname;
            lblInvConName.Content = "Conference Name: " + conferencename;
            lblInvPrice.Content = "Price: " + cost;
        }
    }
}
